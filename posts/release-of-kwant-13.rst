.. title: Release of Kwant 1.3
.. slug: release-of-kwant-13
.. date: 2017-05-24 21:17:10 UTC
.. category: release-announcement
.. type: text

After more than one year of development, we are extremely pleased to announce the release of Kwant 1.3.

.. TEASER_END

Kwant 1.3 supports

* discretizing of continuum Hamiltonians,
* calculating and displaying local densities and currents,
* declaring and using symmetries and conservation laws,
* calculating bulk properties using the kernel polynomial method,
* finalizing builders with multiple translational symmetries,

and has many other improvements that are detailed in the `what's new in Kwant 1.3 </doc/1/pre/whatsnew/1.3>`_ document.

The `installation instructions </install.html>`_ have been updated to explain how to install Kwant 1.3 on computers running GNU/Linux, Mac OS, and Windows.  Note that thanks to the package manager Conda it is now much easier to install Kwant under Mac OS and on Unix accounts without root privileges.

The new version is practically 100% backwards-compatible with scripts written for previous versions of Kwant 1.x.

The Kwant team is happy to welcome Joseph Weston as a member.  We are also grateful to the many contributors without whom this release would not be nearly as good:

* Jörg Behrmann
* Bas Nijholt
* Michał Nowak
* Viacheslav Ostroukh
* Pablo Pérez Piskunow
* Tómas Örn Rosdahl
* Sebastian Rubbert
* Rafał Skolasiński
* Adrien Sorgniard

We would like to hear your feedback at kwant-discuss@kwant-project.org.
