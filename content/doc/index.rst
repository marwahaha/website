Documentation
=============

Tutorial and reference manual
-----------------------------

.. class:: docs-list, button-links

* **Online**:

  + `stable version </doc/1/>`_
  + `development version </doc/dev/>`_

* **Downloads**:

  + `PDF <http://downloads.kwant-project.org/doc/latest.pdf>`_
  + `zipped HTML <http://downloads.kwant-project.org/doc/latest.zip>`_

Interactive online course
-------------------------
The APS March meeting 2016 tutorial “Introduction to Computational Quantum Nanoelectronics” focuses on the physics, but also serves as a crash course on Kwant.  `All the materials are available online </mm16.html>`_ and can be run directly in a web browser, without installing Kwant locally.

Screencast
----------

.. raw:: html

   A brief video introduction of Kwant:

   <ul class="button-links inline-list">
       <li> <a href="kwant-screencast-2014.html">watch</a> </li>
       <li> <a href="http://downloads.kwant-project.org/doc/kwant-screencast-2014.mp4" download>download</a> </li>
   </ul>

Article
-------

This paper (`download PDF <http://downloads.kwant-project.org/doc/kwant-paper.pdf>`__) introduces Kwant in a systematic way and discusses its design and performance (`New J. Phys. 16, 063065 (2014) <http://iopscience.iop.org/1367-2630/16/6/063065/article>`_).
