Quantum transport simulations made easy
=======================================

.. raw:: html

    <div class="workflow-image">
    <object type="image/svg+xml" data="kwant-workflow.svgz" class="img-responsive">kwant-workflow.svgz</object>
    </div>

Kwant is a `free (open source) <https://gitlab.kwant-project.org/kwant/kwant>`_,
powerful, and easy to use Python package for numerical calculations on tight-binding
models with a strong focus on quantum transport.

.. raw:: html

    <div id="recent-posts" class="section col-sm-6">
    <h2> Recent blog posts </h2>

.. post-list::
    :stop: 4

.. raw:: html

    </div>

.. raw:: html

    <div id="kwant-uses" class="section col-sm-5">
    <h2> Use Kwant to... </h2>
    <ul id="kwant-uses-list">
        <li>
        <a href="/about#quantum-hall-effect"> Compute differential conductance </a>
        </li>
        <li>
        <a href="/about#graphene-flake"> Visualize edge states </a>
        </li>
        <li>
        <a href="/about#numerical-experiment-flying-qubit"> Conduct numerical experiments </a>
        </li>
        <li>
        <a href="/doc/1/tutorial/spectrum#band-structure-calculations"> Explore band structure </a>
        </li>
    </div>
