The Kwant community
===================

The Kwant project is an international collaboration.  Everybody is welcome to
participate in the community by asking and replying to questions, reporting
bugs, providing suggestions, and `contributing to the code and documentation
</contribute.html>`_.

A list of `Kwant authors </authors>`_ is included in the documentation.
Please only contact the authors directly for matters that cannot be discussed
in public on one of the mailing lists.


Mailing list
------------

The kwant-discuss mailing list is the main public communication platform for
anything related to Kwant: questions, discussions, development, and
announcements.  (Bug reports may be also sent, but the preferred way is
described further below on this page.)  You can use it in various ways:

- .. raw:: html

    <form class="form-inline"  method="get" action="https://www.mail-archive.com/search">
    <div class="input-group col-md-8">
    <input type="text" class="form-control" placeholder="Search query" size=25 name="q">
    <input type="hidden" name="l" value="kwant-discuss@kwant-project.org">
    <span class="input-group-btn">
    <input class="btn btn-default" type="submit" value="Search kwant-discuss">
    </span>
    </div>
    </form>

- to subscribe and receive new messages by email: either use the form on the
  `kwant-discuss info page
  <https://mailman-mail5.webfaction.com/listinfo/kwant-discuss>`_ or simply send
  a message to kwant-discuss-join@kwant-project.org.  (The subject and content
  are ignored and may be empty.)  To unsubscribe, either use the form at the
  bottom of the info page, or send any message to
  kwant-discuss-leave@kwant-project.org.

- You may follow the list through a web interface that also provides a RSS
  feed: `kwant-discuss on Mail-archive
  <https://www.mail-archive.com/kwant-discuss@kwant-project.org/>`_.

- Whether you are subscribed or not, you may post on the list simply by sending
  email to kwant-discuss@kwant-project.org.

- Please do not hesitate to join the discussion.  It’s easy to reply to a
  posting even if you are not subscribed (or receiving digests): (1) open the
  article in the mail-archive web interface, (2) click on the button “Reply via
  email to” at the bottom of the page, (3) add the above list address under
  "CC".


The `Kwant development mailing list
<https://mailman-mail5.webfaction.com/listinfo/kwant-devel>`_ has been retired
in 2019.  Please use the main list instead, or the other communication channels
listed below (chat and gitlab). The archives of kwant-devel remain searchable:

.. raw:: html

  <form class="form-inline"  method="get" action="https://www.mail-archive.com/search">
  <div class="input-group col-md-8">
  <input type="text" class="form-control" placeholder="Search query" size=25 name="q">
  <input type="hidden" name="l" value="kwant-devel@kwant-project.org">
  <span class="input-group-btn">
  <input class="btn btn-default" type="submit" value="Search kwant-devel">
  </span>
  </div>
  </form>


Announcements mailing list
--------------------------

This read-only list is reserved for important announcements like new releases of
Kwant.  Only a few messages will be sent per year.  These announcements will be also posted on the main mailing list, so there is no need to subscribe to both lists.  We recommend every user of Kwant to subscribe at least to this list in order to stay informed about new developments.

- View archives: `kwant-announce on Mail-archive <https://www.mail-archive.com/kwant-announce@kwant-project.org/>`_.  Mail-archive provides RSS feeds.

- There are two ways to subscribe: either use the form on the `kwant-announce
  info page <https://mailman-mail5.webfaction.com/listinfo/kwant-announce>`_ or
  simply send any message to kwant-announce-join@kwant-project.org.  (The subject
  and content are ignored and may be empty.)  To unsubscribe, either use the
  form at the bottom of the info page, or send any message to
  kwant-announce-leave@kwant-project.org.


Development chat
----------------

The Kwant developer chat is accessible to the community via `Gitter
<https://gitter.im/kwant-project/Lobby>`_.  Come say hi if you would like to get
involved with developing Kwant!


Gitlab instance
---------------

The Kwant project runs a `gitlab instance <https://gitlab.kwant-project.org/>`_
that hosts the `main Kwant code repository
<https://gitlab.kwant-project.org/kwant/kwant>`_ as well as the `repositories of
related projects <https://gitlab.kwant-project.org/kwant>`_.  The gitlab
instance is used for reporting bugs (see next section) and `development
</contribute.html>`_


Reporting bugs
--------------

If you encounter a problem with Kwant, first try to reproduce it with as
simple a system as possible.  Double-check with the documentation that what
you observe is actually a bug in Kwant. If you think it is, please check `the
list of known bugs in Kwant
<https://gitlab.kwant-project.org/kwant/kwant/issues?label_name=bug>`_.  It
may be also a good idea to search or ask on the general mailing list. (You can
use the search box at the top of this page.)

If after checking you still think that you have found a bug, please add it to
the above-mentioned list of bugs by creating an issue with the “bug” label.  A
useful bug report should contain:

- The versions of software you are using: Kwant, Python, operating system, etc.

- A description of the problem, i.e. what exactly goes wrong.  This should
  include any relevant error messages.

- Enough information to reproduce the bug, preferably in the form of a simple
  script.
