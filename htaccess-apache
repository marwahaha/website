<IfModule mod_mime.c>
AddType image/svg+xml svg svgz
AddEncoding gzip svgz
</IfModule>

# Do not treat Python files as CGI scripts.
<FilesMatch \.py$>
  SetHandler default-handler
</FilesMatch>

RewriteEngine on

# We don't need more than that.
RewriteOptions MaxRedirects=2

# Redirect /foo/index.html to /foo/
RewriteRule ^(|.*/)index\.html$ /$1 [R=301,L]

# Redirect /foo/bar.html to /foo/bar only if a html file was requested by the
# browser.  Without the condition the following rewrite rule leads to a
# redirection loop.
RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ /.*\.html\ HTTP/
RewriteRule ^(.*)\.html$ /$1 [R=301,L]

# Rewrite /foo/bar to /foo/bar.html if the latter exists.
RewriteCond %{REQUEST_FILENAME}.html -f
RewriteRule ^(.*[^/])$ /$1.html [L]

# Remove trailing slash if the requested directory does not exist.
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)/$  /$1 [R=301,L]

Redirect 302 /contact /doc/latest/pre/authors
Redirect 302 /authors /doc/latest/pre/authors
Redirect 302 /citing /doc/latest/pre/citing
Redirect 302 /cite /doc/latest/pre/citing
Redirect 302 /license /doc/latest/pre/license
Redirect 302 /download /install

# Redirect old tutorial URLs to new ones.
# The tutorial numbers refer to the tutorial enumeration in Kwant 1.2.x
Redirect 302 /doc/1/tutorial/tutorial1 /doc/1/tutorial/first_steps
Redirect 302 /doc/1/tutorial/tutorial2 /doc/1/tutorial/spin_potential_shape
Redirect 302 /doc/1/tutorial/tutorial3 /doc/1/tutorial/spectrum
Redirect 302 /doc/1/tutorial/tutorial4 /doc/1/tutorial/graphene
Redirect 302 /doc/1/tutorial/tutorial5 /doc/1/tutorial/superconductor
Redirect 302 /doc/1/tutorial/tutorial6 /doc/1/tutorial/plotting
